# without-same

Copy an array but leave out duplicate elements by checking a given key on each element.

## API

### `operand::without(identity)(blacklist)`

Returns an `Array` containing all elements that exist in `operand` except those with an identical key/value in `blacklist`.

Designed for use with `::` function bind syntax, as the `this` property should be the Array to filter.

#### operand

`Array` instance to filter.

#### identity

`String` key name of the property by which to compare elements.

#### blacklist

`Array` containing elements to check for matching values.

## Use Case

```js
import without from 'without-same'

const snacks = [{name: 'banana'}, {name: 'cookie'}]
const sugary = [{name: 'cookie'}]

const healthy = snacks::without('name')(sugary)
// -> [{name: 'banana'}]
```

## Peer Dependencies

This package uses Babel to transpile its code to be compatible with the node version it is installed on. Ensure you have installed the necessary peer dependencies.

```
npm install babel-cli babel-preset-env babel-plugin-transform-function-bind
```

On some older Node environments, e.g. 4.x and earlier, you may also require system-wide polyfills.

```
npm install babel-polyfill
```

And then load the `babel-polyfill` library before loading this package.

## Colophon

Made by Sebastiaan Deckers in Singapore 🇸🇬
