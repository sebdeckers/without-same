export default function withoutSame (property) {
  return (blacklist) => this.filter(
    (accept) => blacklist.every(
      (reject) => accept[property] !== reject[property]
    )
  )
}
