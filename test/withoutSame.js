import test from 'ava'
import withoutSame from '..'

test.only('withoutSame should not suck', (t) => {
  const whitelist = [{a: 123}, {b: 456}, {a: 456}]
  const blacklist = [{a: 123}]
  const actual = whitelist::withoutSame('a')(blacklist)
  const expected = [{b: 456}, {a: 456}]
  t.deepEqual(actual, expected)
})

test.only('withoutSame should not suck', (t) => {
  const snacks = [{name: 'banana'}, {name: 'cookie'}]
  const sugary = [{name: 'cookie'}]
  const actual = snacks::withoutSame('name')(sugary)
  const expected = [{name: 'banana'}]
  t.deepEqual(actual, expected)
})
